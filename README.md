# J!XYZ API Definitions

In this repository, you can find the definitions for each of the three supported
API types. You can use this to explore the API, make suggestions for
improvements through merge requests, and contribute new API definitions.

If you're creating a new implementation of one of these in your favorite stack,
then you can use these as your canonical reference.

* The [`rest.jxyz.yaml`](./rest.jxyz.yaml) file contains the REST API definition
  using the OpenAPI standard.
* (coming soon) The `jsonapi.jxyz.yaml` file contains the JSON:API definition
  using the OpenAPI standard.
* (coming soon) The `jxyz.graphql` file contains the GraphQL API using the
  GraphQL Schema Definition Language.

You can check out the build artifacts for the JSON versions of each of the
OpenAPI YAML files.
