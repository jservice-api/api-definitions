from yaml import dump, Dumper, load, Loader

def generate_openapi_dev_version(source: str) -> str:
    content = load(source, Loader=Loader)
    if "servers" not in content:
        content["servers"] = []
    content["servers"].insert(0, {
        "url": "http://localhost:8080",
        "description": "The local development server",
    })
    return dump(content, Dumper=Dumper)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(
                        prog="generate_openapi_dev_version",
                        description="Generate an OpenAPI document with localhost")
    parser.add_argument("source")
    parser.add_argument("destination")
    args = parser.parse_args()
    with open(args.source, mode="r", encoding="utf-8") as source:
        content = source.read()
        modified = generate_openapi_dev_version(content)
        with open(args.destination, mode="w", encoding="utf-8") as destination:
            destination.write(modified)
